# Api endpoints

### New User registration

Request
```URL
POST /v1/users
```

Body
```json
{"user": {"metrics": "some string"}}
```

Response
```json
{"access_token": "68af5ee6-e888-49c8-b58f-995f8b83abf8"}
```

### Get statistic

Request
```URL
GET /v1/statistic
```

Headers
```HTTP
HTTP_AUTHORIZATION: Token token=<access-token>
```

Response
```json
{"safe_sites_count": <number>, "unsafe_sites_count": <number>}
```

### Analyze site

Request

```URL
POST /v1/documents
```
Headers
```HTTP
HTTP_AUTHORIZATION: Token token=<access-token>
```

Body
```json
{"document": {"content": "... text ..."}, "url": "<URL>"}
```

Response:
```json
{"safe": <true/false/null>}
```

