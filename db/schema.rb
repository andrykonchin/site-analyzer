# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160923165513) do

  create_table "documents", force: :cascade do |t|
    t.string   "sha1_hash",  null: false
    t.text     "content",    null: false
    t.boolean  "safe"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "documents", ["sha1_hash"], name: "index_documents_on_sha1_hash"

  create_table "users", force: :cascade do |t|
    t.string   "metrics",    null: false
    t.string   "token",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["token"], name: "index_users_on_token"

  create_table "visits", force: :cascade do |t|
    t.string   "url",         null: false
    t.integer  "user_id",     null: false
    t.integer  "document_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "visits", ["document_id"], name: "index_visits_on_document_id"
  add_index "visits", ["user_id"], name: "index_visits_on_user_id"

end
