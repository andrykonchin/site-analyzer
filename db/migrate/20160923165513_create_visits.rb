class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.string :url, null: false
      t.integer :user_id, index: true, null: false
      t.integer :document_id, index: true, null: false

      t.timestamps
    end

    add_foreign_key :visits, :users
    add_foreign_key :visits, :documents
  end
end
