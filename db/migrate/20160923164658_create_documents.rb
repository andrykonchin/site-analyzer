class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :sha1_hash, null: false
      t.text :content, null: false
      t.boolean :safe
      t.timestamps

      t.index :sha1_hash
    end
  end
end
