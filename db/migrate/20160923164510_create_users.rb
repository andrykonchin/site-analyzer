class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :metrics, null: false
      t.string :token, null: false
      t.timestamps

      t.index :token
    end
  end
end
