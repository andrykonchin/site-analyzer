class AnalizeContentJob < ActiveJob::Base
  queue_as :default

  def perform(document_id)
    document = Document.find(document_id)
    safe = AnalizeContent.call(document.content)
    document.update_attributes!(safe: safe)
  end
end
