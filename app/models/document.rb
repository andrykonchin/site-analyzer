class Document < ActiveRecord::Base
  def analyzed?
    !safe.nil?
  end
end
