class Visit < ActiveRecord::Base
  belongs_to :user
  belongs_to :document

  scope :safe,   -> () { joins(:document).where(documents: {safe: true}) }
  scope :unsafe, -> () { joins(:document).where(documents: {safe: false}) }
end
