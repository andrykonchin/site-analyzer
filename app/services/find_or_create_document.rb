require 'digest/sha1'

class FindOrCreateDocument
  def self.call(content)
    sha1_hash = Digest::SHA1.hexdigest(content)
    document = Document.where(sha1_hash: sha1_hash).first
    document ||= Document.create!(content: content, sha1_hash: sha1_hash, safe: nil)
    document
  end
end
