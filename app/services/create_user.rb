class CreateUser
  def self.call(attrs)
    User.create!(metrics: attrs[:metrics], token: SecureRandom.uuid)
  end
end
