class AnalizeContent
  def self.call(str)
    str.length.even?
  end
end
