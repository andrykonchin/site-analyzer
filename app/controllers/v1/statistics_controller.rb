class V1::StatisticsController < ApplicationController
  def show
    authenticate!

    safe_sites_count = current_user.visits.safe.count
    unsafe_sites_count = current_user.visits.unsafe.count

    render json: {
      safe_sites_count: safe_sites_count,
      unsafe_sites_count: unsafe_sites_count
    }
  end
end
