class V1::UsersController < ApplicationController
  def create
    user = CreateUser.call(params[:user])
    render json: {access_token: user.token}
  end
end
