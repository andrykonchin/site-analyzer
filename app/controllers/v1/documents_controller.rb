class V1::DocumentsController < ApplicationController
  def create
    authenticate!

    document = FindOrCreateDocument.call(params[:document][:content])
    Visit.create(user: current_user, document: document, url: params[:url])

    unless document.analyzed?
      AnalizeContentJob.perform_later(document.id)
    end

    render json: {safe: document.safe}
  end
end
