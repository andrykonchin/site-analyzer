require 'warden'

class AuthenticationTokenStrategy < ::Warden::Strategies::Base
  def valid?
    authentication_token
  end

  def authenticate!
    user = User.where(token: authentication_token.to_s).first
    user.nil? ? fail!('strategies.authentication_token.failed') : success!(user)
  end

  private

  def authentication_token
    header = request.env['HTTP_AUTHORIZATION']
    if header && header[ActionController::HttpAuthentication::Token::TOKEN_REGEX]
      params = ActionController::HttpAuthentication::Token.token_params_from(header)
      params.flatten[1]
    end
  end
end

