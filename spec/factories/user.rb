FactoryGirl.define do
  factory :user do
    metrics '{}'
    token { Faker::Number.hexadecimal(40) }
  end
end
