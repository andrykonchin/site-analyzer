FactoryGirl.define do
  factory :visit do
    association :user
    association :document

    url { Faker::Internet.url }
  end
end

