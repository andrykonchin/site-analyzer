require 'digest/sha1'

FactoryGirl.define do
  factory :document do
    content { Faker::Lorem.paragraph }
    sha1_hash { Digest::SHA1.hexdigest(content) }

    trait :safe do
      safe true
    end

    trait :unsafe do
      safe false
    end
  end
end
