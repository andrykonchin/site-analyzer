module Requests
  module DefaultHeaders
    def default_headers(token:)
      {'HTTP_AUTHORIZATION' => "Token token=#{token}"}
    end
  end
end

