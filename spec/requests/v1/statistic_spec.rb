require 'rails_helper'

describe 'Sites statistic' do
  context 'when token is passed' do
    subject do
      get '/v1/statistic', {}, default_headers(token: user.token)
      assert_response :success
    end

    let(:user) { create(:user) }

    before do
      create(:visit, user: user, document: create(:document, :safe))
      create(:visit, user: user, document: create(:document, :unsafe))
      create(:visit, user: user, document: create(:document, :unsafe))
    end

    it 'responds with safe and not safe sites count' do
      subject
      json = JSON.parse(response.body)
      expect(json).to eq({
        'safe_sites_count' => 1,
        'unsafe_sites_count' => 2
      })
    end
  end

  context 'when token is not passed' do
    it 'responds with 401 Unauthorized' do
      get '/v1/statistic', {}, {}
      assert_response 401
    end
  end
end
