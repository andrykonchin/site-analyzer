require 'rails_helper'

describe 'Users' do
  describe 'Creating of user' do
    it 'creates new user' do
      expect {
        post '/v1/users', user: {metrics: ['Chrome/53.0.2785.116']}
        assert_response :success
      }.to change { User.count }
    end

    it 'responds with access_token' do
      post '/v1/users', user: {metrics: ['Chrome/53.0.2785.116']}
      assert_response :success
      json = JSON.parse(response.body)
      expect(json['access_token']).to be_present
    end
  end

end
