require 'rails_helper'

describe 'Documents' do
  describe 'Safety check' do
    context 'when document is already analyzed and safe' do
      subject do
        post '/v1/documents', {document: {content: document.content}, url: 'http://google.com'},
          default_headers(token: user.token)
        assert_response :success
      end

      let(:user) { create(:user) }
      let(:document) { create(:document, :safe) }

      it 'responds with safe=true' do
        subject
        json = JSON.parse(response.body)
        expect(json['safe']).to eq(true)
      end

      it 'tracks URL' do
        expect { subject }.to change { user.visits.count }.by(1)
      end
    end

    context 'when document is already analyzed and unsafe' do
      subject do
        post '/v1/documents', {document: {content: document.content}, url: 'http://google.com'},
          default_headers(token: user.token)
        assert_response :success
      end

      let(:user) { create(:user) }
      let(:document) { create(:document, :unsafe) }

      it 'responds with safe=false' do
        subject
        json = JSON.parse(response.body)
        expect(json['safe']).to eq(false)
      end

      it 'tracks URL' do
        expect { subject }.to change { user.visits.count }.by(1)
      end
    end

    context 'when document is not already analyzed' do
      subject do
        post '/v1/documents', {document: {content: 'some content'}, url: 'http://google.com'},
          default_headers(token: user.token)
        assert_response :success
      end

      let(:user) { create(:user) }

      it 'responds with safe=null' do
        subject
        json = JSON.parse(response.body)
        expect(json).to eq({'safe' => nil})
      end

      it 'analyzes document and saves result' do
        expect { subject }.to change { Document.count }.by(1)
        expect(Document.last.safe).not_to be_nil
      end

      it 'tracks URL' do
        expect { subject }.to change { user.visits.count }.by(1)
      end
    end

    context 'when token is not passed' do
      it 'responds with 401 Unauthorized' do
        post '/v1/documents', {document: {content: 'some content'}, url: 'http://google.com'}, {}
        assert_response 401
      end
    end
  end
end
