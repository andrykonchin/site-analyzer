require 'spec_helper'
require './app/services/analize_content'

describe AnalizeContent do
  describe '.call' do
    it 'returns true if content length is even' do
      expect(described_class.call('1234')).to eq true
    end

    it 'returns true if content length is odd' do
      expect(described_class.call('12345')).to eq false
    end
  end
end
